<?php

namespace CodingMs\Commands\Command;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use \TYPO3\CMS\Extbase\Mvc\Controller\CommandController;

/**
 * Base commands
 *
 * @package commands
 *
 */
class BaseCommandController extends CommandController
{

    /**
     * Base folder
     * @var string
     */
    protected $baseFolder = '';

    /**
     * @var string
     */
    protected $logFilename = '';

    /**
     * @var \TYPO3\CMS\Core\Messaging\FlashMessageService $flashMessageService
     */
    protected $flashMessageService = null;

    /**
     * @var \TYPO3\CMS\Core\Messaging\FlashMessageQueue $defaultFlashMessageQueue
     */
    protected $defaultFlashMessageQueue = null;

    /**
     * @var float
     */
    protected $executionStartTime = 0.0;

    /**
     * Initialize the controller
     * Prepares all the stuff
     * @return bool
     * @throws \TYPO3\CMS\Core\Exception
     */
    protected function initialize()
    {
        $success = true;
        // Create filename for log file
        $logFilenameExtension = GeneralUtility::camelCaseToLowerCaseUnderscored($this->commandMethodName);
        $logFilenameExtension = str_replace('_', '-', $logFilenameExtension);
        $this->logFilename = date('Y-m-d_H-i-s') . '_command-' . $logFilenameExtension . '.log';
        // Get base folder
        $this->baseFolder = GeneralUtility::getFileAbsFileName('tx_commands/');
        if (!file_exists($this->baseFolder) && $success) {
            $success = GeneralUtility::mkdir($this->baseFolder);
        }
        // Create required directories
        $logDir = GeneralUtility::getFileAbsFileName($this->baseFolder . 'log/');
        if (!file_exists($logDir) && $success) {
            $success = GeneralUtility::mkdir($logDir);
            $this->log("Create folder: '" . $logDir . "'", 'info');
        }
        // .htaccess file check
        if (!file_exists($logDir . '.htaccess') && $success) {
            $htaccess = 'deny from all';
            $success = (bool)file_put_contents($logDir . '.htaccess', $htaccess);
        }
        return $success;
    }

    /**
     * Log some messages
     *
     * @param string $message Message text
     * @param int $severity Severity: 0 is info, 1 is notice, 2 is warning, 3 is fatal error, -1 is "OK" message
     * @throws \TYPO3\CMS\Core\Exception
     */
    protected function log($message, $severity = 0)
    {
        $message .= ' (Time: ' . (microtime(true) - $this->executionStartTime) . ')';
        if (TYPO3_DLOG && $severity != 'log') {
            // Severity: 0 is info, 1 is notice, 2 is warning, 3 is fatal error, -1 is "OK" message
            $severityId = 2;
            switch ($severity) {
                case 'info':
                    $severityId = 0;
                    break;
                case 'notice':
                    $severityId = 1;
                    break;
                case 'warning':
                    $severityId = 2;
                    break;
                case 'error':
                    $severityId = 3;
                    break;
                case 'ok':
                    $severityId = -1;
                    break;
            }
            GeneralUtility::devLog($message, 'commands', $severityId);
        }
        // Executed by scheduler?!
        if (PHP_SAPI != 'cli' && $severity != 'log') {
            if ($this->flashMessageService === null) {
                $this->flashMessageService = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Messaging\\FlashMessageService');
            }
            if ($this->defaultFlashMessageQueue === null) {
                $this->defaultFlashMessageQueue = $this->flashMessageService->getMessageQueueByIdentifier();
            }
            /**
             * Severity classes
             * self::NOTICE => 'notice',
             * self::INFO => 'info',
             * self::OK => 'success',
             * self::WARNING => 'warning',
             * self::ERROR => 'danger'
             */
            $severityClass = FlashMessage::WARNING;
            switch ($severity) {
                case 'notice':
                    $severityClass = FlashMessage::NOTICE;
                    break;
                case 'info':
                    $severityClass = FlashMessage::INFO;
                    break;
                case 'ok':
                    $severityClass = FlashMessage::OK;
                    break;
                case 'warning':
                    $severityClass = FlashMessage::WARNING;
                    break;
                case 'error':
                    $severityClass = FlashMessage::ERROR;
                    break;
            }
            /** @var \TYPO3\CMS\Core\Messaging\FlashMessage $flashMessageInfo */
            $flashMessageInfo = GeneralUtility::makeInstance(
                'TYPO3\\CMS\\Core\\Messaging\\FlashMessage',
                $message,
                '',
                $severityClass);
            $this->defaultFlashMessageQueue->enqueue($flashMessageInfo);
        } else {
            if ($severity != 'log') {
                echo $message . "\n";
            }
        }
        // Always push to log file
        file_put_contents($this->baseFolder . 'log/' . $this->logFilename, $message . "\n", FILE_APPEND);
    }
}