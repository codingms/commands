<?php

namespace CodingMs\Commands\Command;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\CommandUtility;

/**
 * File system commands
 *
 * @package commands
 *
 */
class FileSystemCommandController extends BaseCommandController
{

    /**
     * @var array
     */
    protected $folder = [];

    /**
     * Initialize the controller
     * Prepares all the stuff
     * @return bool
     * @throws \TYPO3\CMS\Core\Exception
     */
    protected function initialize()
    {
        $success = parent::initialize();
        // Create required directories
        $exportDir = GeneralUtility::getFileAbsFileName($this->baseFolder . 'export/');
        if (!file_exists($exportDir) && $success) {
            $success = GeneralUtility::mkdir($exportDir);
            $this->log("Create folder: '" . $exportDir . "'", 'info');
        }
        // .htaccess file check
        if (!file_exists($exportDir . '.htaccess') && $success) {
            $htaccess = 'deny from all';
            $success = (bool)file_put_contents($exportDir . '.htaccess', $htaccess);
        }
        return $success;
    }

    /**
     * Create a tart from a folder using Archive_Tar (PEAR library)
     *
     * This is the description
     *
     * @param string $folder Comma-separated list of folder, for example: fileadmin,uploads/tx_commands
     * @throws \TYPO3\CMS\Core\Exception
     */
    public function tarPhpFolderCommand($folder = 'fileadmin,typo3conf,uploads')
    {
        $this->executionStartTime = microtime(true);
        // Catch all exceptions!
        try {
            // initialize command
            if (!$this->initialize()) {
                return;
            }
            // Extract folder string
            $folderArray = GeneralUtility::trimExplode(',', $folder, true);
            $this->log('Passed folder: ' . implode(',', $folderArray), 'info');
            if (count($folderArray) > 0) {
                include "Archive/Tar.php";
                foreach ($folderArray as $zipFolder) {
                    $absZipFolder = GeneralUtility::getFileAbsFileName($zipFolder);
                    if (file_exists($absZipFolder)) {
                        // Export filename and storage folder
                        $suffix = str_replace('//', '-', $zipFolder);
                        $exportFilename = date('Y-m-d_H-i-s') . '_fs-export_' . $suffix . '.tar';
                        $exportFilename = $this->baseFolder . 'export/' . $exportFilename;
                        $this->log('Zip Folder \'' . $absZipFolder . '\' into \'' . $exportFilename . '\'!', 'info');
                        $archive = new \Archive_Tar($exportFilename, true);
                        $archive->createModify([$absZipFolder], "", $absZipFolder);
                    } else {
                        $this->log('Folder \'' . $zipFolder . '\' not found!', 'error');
                    }
                }
            } else {
                $this->log('Please pass one or more folder!', 'error');
            }
        } catch (\Exception $e) {
            $this->log('Exception: ' . $e->getMessage(), 'error');
            $this->log('Exception-Code: ' . $e->getCode(), 'error');
            $this->log('Exception-Line: ' . $e->getLine(), 'error');
            $this->log('Exception-Trace: ' . $e->getTraceAsString(), 'error');
        }
        $executionEndTime = microtime(true);
        $executionTime = $executionEndTime - $this->executionStartTime;
        $this->log('Execution time: ' . $executionTime, 'info');
    }

    /**
     * Create a tar/zip from a folder
     *
     * This is the description
     *
     * @param string $folder Comma-separated list of folder, for example: fileadmin,uploads/tx_commands
     * @throws \TYPO3\CMS\Core\Exception
     */
    public function tarFolderCommand($folder = 'fileadmin,typo3conf,uploads')
    {
        $this->executionStartTime = microtime(true);
        // Catch all exceptions!
        try {
            // initialize command
            if (!$this->initialize()) {
                return;
            }
            // Extract folder string
            $folderArray = GeneralUtility::trimExplode(',', $folder, true);
            $this->log('Passed folder: ' . implode(',', $folderArray), 'info');
            //
            $userBin = 'tar';
            $absUserBin = CommandUtility::getCommand(escapeshellcmd($userBin));
            if (!is_string($absUserBin)) {
                throw new \RuntimeException('Binary ' . $absUserBin . ' not found', 1488631746);
            } else {
                $this->log('/usr/bin/' . $userBin . ' found!', 'ok');
            }
            if (count($folderArray) > 0) {
                foreach ($folderArray as $zipFolder) {
                    $absZipFolder = GeneralUtility::getFileAbsFileName($zipFolder);
                    if (file_exists($absZipFolder)) {
                        // Export filename and storage folder
                        $suffix = str_replace('//', '-', $zipFolder);
                        $exportFilename = date('Y-m-d_H-i-s') . '_fs-export_' . $suffix . '.tar.gz';
                        $exportFolder = GeneralUtility::getFileAbsFileName('tx_commands/export/');
                        $exportFile = ' -zcf ' . $exportFolder . $exportFilename;
                        $target = ' -C ' . $absZipFolder . ' .';
                        $shellCommand = $absUserBin . $exportFile . $target . ' 2>&1';
                        // Execute
                        $output = [];
                        $returnValue = 0;
                        $this->log('Execute: ' . $shellCommand, 'ok');
                        CommandUtility::exec($shellCommand, $output, $returnValue);
                        // 126 -> sh: /bin/tar: Permission denied
                        $this->log($shellCommand . ' exited with ' . $returnValue, 'ok');
                        $this->log('Output was: ' . implode(' ', $output), 'ok');
                    } else {
                        $this->log('Folder \'' . $zipFolder . '\' not found!', 'error');
                    }
                }
            } else {
                $this->log('Please pass one or more folder!', 'error');
            }
        } catch (\Exception $e) {
            $this->log('Exception: ' . $e->getMessage(), 'error');
            $this->log('Exception-Code: ' . $e->getCode(), 'error');
            $this->log('Exception-Line: ' . $e->getLine(), 'error');
            $this->log('Exception-Trace: ' . $e->getTraceAsString(), 'error');
        }
        $executionEndTime = microtime(true);
        $executionTime = $executionEndTime - $this->executionStartTime;
        $this->log('Execution time: ' . $executionTime, 'info');
    }

}