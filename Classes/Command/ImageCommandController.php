<?php
namespace CodingMs\Commands\Command;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\CommandUtility;

/**
 * Image commands
 *
 * @package commands
 *
 */
class ImageCommandController extends BaseCommandController
{

    /**
     * @var array
     */
    protected $folder = array();

    /**
     * Initialize the controller
     * Prepares all the stuff
     * @return bool
     * @throws \TYPO3\CMS\Core\Exception
     */
    protected function initialize()
    {
        $success = parent::initialize();
        // ...
        return $success;
    }

    /**
     * Optimize images from storages
     *
     * Optimizes all JPEG images in _processed_-Folder using jpegoptim!
     *
     * @param int $quality Image quality (0-100 percent)
     * @throws \TYPO3\CMS\Core\Exception
     */
    public function optimizeJpegCommand($quality=70)
    {
        $this->executionStartTime = microtime(true);
        // Catch all exceptions!
        try {
            // initialize command
            if (!$this->initialize()) {
                return;
            }
            //
            $userBin = 'jpegoptim';
            $absUserBin = CommandUtility::getCommand(escapeshellcmd($userBin));
            if (!is_string($absUserBin)) {
                throw new \RuntimeException('Binary ' . $absUserBin . ' not found', 1488631746);
            }
            else {
                $this->log('/usr/bin/' . $userBin . ' found!', 'ok');
            }
            // Get storages and contained processed folder
            /** @var \TYPO3\CMS\Core\Resource\ResourceStorage[] $storages */
            $storages = GeneralUtility::makeInstance('TYPO3\CMS\Core\Resource\StorageRepository')->findAll();
            foreach ($storages as $storage) {
                $this->log('Process storage: ' . $storage->getName(), 'info');
                /** @var \TYPO3\CMS\Core\Resource\Folder $folder */
                foreach($storage->getProcessingFolders() as $folder) {
                    $absFolder = GeneralUtility::getFileAbsFileName($storage->getPublicUrl($folder));
                    $this->log('  Process folder: ' . $absFolder, 'info');
                    $objects = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($absFolder), \RecursiveIteratorIterator::SELF_FIRST);
                    /**
                     * @var string $name
                     * @var \SplFileInfo $object
                     */
                    foreach($objects as $name => $object){
                        if($object->isFile()) {
                            $fileExtension = trim(strtolower($object->getExtension()));
                            if($fileExtension == 'jpg' || $fileExtension == 'jpeg') {
                                $shellCommand = $absUserBin . ' ' . $object->getRealPath() . ' --strip-all 2>&1';
                                // Execute
                                $output = array();
                                $returnValue = 0;
                                $this->log('Execute: ' . $shellCommand, 'ok');
                                CommandUtility::exec($shellCommand, $output, $returnValue);
                                $this->log($shellCommand . ' exited with ' . $returnValue, 'ok');
                                $this->log('Output was: ' . implode(' ', $output), 'ok');
                                $this->log('    Process file: ' . $shellCommand, 'info');
                            }
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            $this->log('Exception: ' . $e->getMessage(), 'error');
            $this->log('Exception-Code: ' . $e->getCode(), 'error');
            $this->log('Exception-Line: ' . $e->getLine(), 'error');
            $this->log('Exception-Trace: ' . $e->getTraceAsString(), 'error');
        }
        $executionEndTime = microtime(true);
        $executionTime = $executionEndTime - $this->executionStartTime;
        $this->log('Execution time: ' . $executionTime, 'info');
    }

    /**
     * Optimize images from storages
     *
     * Optimizes all PNG images in _processed_-Folder using optipng!
     *
     * @throws \TYPO3\CMS\Core\Exception
     */
    public function optimizePngCommand()
    {
        $this->executionStartTime = microtime(true);
        // Catch all exceptions!
        try {
            // initialize command
            if (!$this->initialize()) {
                return;
            }
            //
            $userBin = 'optipng';
            $absUserBin = CommandUtility::getCommand(escapeshellcmd($userBin));
            if (!is_string($absUserBin)) {
                throw new \RuntimeException('Binary ' . $absUserBin . ' not found', 1488631746);
            }
            else {
                $this->log('/usr/bin/' . $userBin . ' found!', 'ok');
            }
            // Get storages and contained processed folder
            /** @var \TYPO3\CMS\Core\Resource\ResourceStorage[] $storages */
            $storages = GeneralUtility::makeInstance('TYPO3\CMS\Core\Resource\StorageRepository')->findAll();
            foreach ($storages as $storage) {
                $this->log('Process storage: ' . $storage->getName(), 'info');
                /** @var \TYPO3\CMS\Core\Resource\Folder $folder */
                foreach($storage->getProcessingFolders() as $folder) {
                    $absFolder = GeneralUtility::getFileAbsFileName($storage->getPublicUrl($folder));
                    $this->log('  Process folder: ' . $absFolder, 'info');
                    $objects = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($absFolder), \RecursiveIteratorIterator::SELF_FIRST);
                    /**
                     * @var string $name
                     * @var \SplFileInfo $object
                     */
                    foreach($objects as $name => $object){
                        if($object->isFile()) {
                            $fileExtension = trim(strtolower($object->getExtension()));
                            if($fileExtension == 'png') {
                                $shellCommand = $absUserBin . ' optipng -o2 -strip all ' . $object->getRealPath() . ' 2>&1';
                                // Execute
                                $output = array();
                                $returnValue = 0;
                                $this->log('Execute: ' . $shellCommand, 'ok');
                                CommandUtility::exec($shellCommand, $output, $returnValue);
                                $this->log($shellCommand . ' exited with ' . $returnValue, 'ok');
                                $this->log('Output was: ' . implode(' ', $output), 'ok');
                                $this->log('    Process file: ' . $shellCommand, 'info');
                            }
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            $this->log('Exception: ' . $e->getMessage(), 'error');
            $this->log('Exception-Code: ' . $e->getCode(), 'error');
            $this->log('Exception-Line: ' . $e->getLine(), 'error');
            $this->log('Exception-Trace: ' . $e->getTraceAsString(), 'error');
        }
        $executionEndTime = microtime(true);
        $executionTime = $executionEndTime - $this->executionStartTime;
        $this->log('Execution time: ' . $executionTime, 'info');
    }

}