# Additional Commands for TYPO3
This extension provides you some useful commands for the TYPO3 Scheduler

Features:

*	Automatic folder zipping (shell tar, PHP/PEAR tar)
*	Automatic database export
*	Automatic image optimization (jpeg, png)

## Configuration


```text
86400 - 1 Day
604800 - 1 Week
1209600 - 2 Week
```


## Automatic image optimization (jpeg, png)

Optimizes PNG and JPEG images in all `_proccessed_` folder.

>	**Notice:**
>
>	Please be sure to have a backup of your image files. Using on own risk!
>	For creating a backup, you can use the `FileSystem:zpFolder` command.


## FAQ

### Find binaries path

```text
type -a tar
```

```text
whereis tar
```

### Provide some binaries pathes

```text
'SYS' => [
        'binPath' => '/usr/bin/,/bin/',
        'binSetup' => 'tar=/bin/tar',
        ...
```

### Known hoster and settings

```text
All-Inkl.:
- tar=/bin/tar



```

### FileSystem:zipFolder exits with: 126 - Permission denied

This happens for example by using All-Inkl as hoster. Try to execute the tar commands by using the shell (SSH).

### Database export exits with: mysqldump: Couldn't execute 'SET OPTION SQL_QUOTE_SHOW_CREATE=1'

The reason for this is that MySQL 5.6 has removed support for "SET OPTION" and your mysql client tools are probably on 
older version. Most likely 5.5 or 5.1. There is more info about this issue on MySQL bugs website: 
http://bugs.mysql.com/bug.php?id=66765

Solution: http://www.markomedia.com.au/mysqldump-mysql-5-6-problem-solved/