# Commands Change-Log


### 2019-05-08  Thomas Deuling  <typo3@coding.ms>

*   [TASK] Change export folder from /typo3temp/tx_commands/ to /tx_commands/.
*   [TASK] Hide sensitive data from log files.
*   [TASK] Create a more detailed log filename.
*   [FEATURE] Add command for creating a tar by using Archive_Tar PEAR library.

## 2017-05-09  Release of 1.0.0

*   [TASK] Initial import.